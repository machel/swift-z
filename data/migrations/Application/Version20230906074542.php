<?php

declare(strict_types=1);

namespace Migrations\Application;

use Application\DomainService\PasswordProcessor;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230906074542 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fill up users table with data';
    }

    public function up(Schema $schema): void
    {
        //$pass = PasswordProcessor::hashPassword('TajnoS--07');
        $pass = '$2y$10$4dVBsSpxolZSXqw083G5rucyGNPDhpiIyCSjTkZn4l2jtt.iMjNBm';

        foreach ($this->emailsToInsert() as $email) {
            $this->addSql('INSERT INTO users (email, password, force_password_change, created_at) VALUES ("' . $email . '", "' . $pass . '", 1, now())');
        }
    }

    public function down(Schema $schema): void
    {
        foreach ($this->emailsToInsert() as $email) {
            $this->addSql('DELETE FROM users WHERE email = "' . $email . '"');
        }
    }

    private function emailsToInsert(): array
    {
        return [
            "kajko@mirmilowo.pl",
            "kokosz@mirmilowo.pl",
            "lamignat@mirmilowo.pl",
            "i@do.uk",
            "i@dont.uk",
        ];
    }
}

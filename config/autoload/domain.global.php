<?php

return [
    'domain' => [
        'password_requirements' => [
            'minLength' => 8,
            'minUppercases' => 2,
            'minLowercases' => 2,
            'minDigits' => 2,
            'minSpecialchars' => 2,
        ],
        'check_new_password_against_n_former_passwords' => 10,
        'password_expiration_in_days' => 12,
        'email_directory' => __DIR__ . '/../../../../data/mail',
    ],
];

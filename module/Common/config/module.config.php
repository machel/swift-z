<?php

declare(strict_types=1);

namespace Common;

use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'service_manager' => [
        'factories' => [
            Responder::class => InvokableFactory::class,
        ],
    ],
];

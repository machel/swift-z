<?php

declare(strict_types=1);

namespace Common\Validator;

use Laminas\Validator\AbstractValidator;
use Laminas\Validator\StringLength;

class PasswordValidator extends AbstractValidator
{
    const LENGTH = 'length';
    const UPPER = 'upper';
    const LOWER = 'lower';
    const DIGIT = 'digit';
    const SPECIAL = 'special';

    public int $minLength = 4;
    public int $minUppercases = 1;
    public int $minLowercases = 1;
    public int $minDigits = 1;
    public int $minSpecialchars = 1;

    protected $messageTemplates = [
        self::LENGTH => "password must be at least '%minLen%' characters in length",
        self::UPPER  => "password must contain at least '%minUpp%' uppercase letters",
        self::LOWER  => "password must contain at least '%minLow%' lowercase letters",
        self::DIGIT  => "password must contain at least '%minDig%' digit characters",
        self::SPECIAL  => "password must contain at least '%minSpec%' special characters",
    ];

    protected $messageVariables = [
        'minLen' => 'minLength',
        'minUpp' => 'minUppercases',
        'minLow' => 'minLowercases',
        'minDig' => 'minDigits',
        'minSpec' => 'minSpecialchars',
    ];

    public function __construct(array $options = [])
    {
        parent::__construct($options);

        if (isset($options['minLength'])) {
            $this->minLength = $options['minLength'];
        }
        if (isset($options['minUppercases'])) {
            $this->minUppercases = $options['minUppercases'];
        }
        if (isset($options['minLowercases'])) {
            $this->minLowercases = $options['minLowercases'];
        }
        if (isset($options['minDigits'])) {
            $this->minDigits = $options['minDigits'];
        }
        if (isset($options['minSpecialchars'])) {
            $this->minSpecialchars = $options['minSpecialchars'];
        }
    }

    public function isValid($value): bool
    {
        $this->setValue($value);

        $lengthValidator = new StringLength(['min' => $this->minLength]);

        $isValid = true;

        if (!$lengthValidator->isValid($value)) {
            $this->error(self::LENGTH);
            $isValid = false;
        }
        if (preg_match_all('/[A-Z]/', $value) < $this->minUppercases) {
            $this->error(self::UPPER);
            $isValid = false;
        }
        if (preg_match_all('/[a-z]/', $value) < $this->minLowercases) {
            $this->error(self::LOWER);
            $isValid = false;
        }
        if (preg_match_all('/\d/', $value) < $this->minDigits) {
            $this->error(self::DIGIT);
            $isValid = false;
        }
        if (preg_match_all('/\W/', $value) < $this->minSpecialchars) {
            $this->error(self::SPECIAL);
            $isValid = false;
        }

        return $isValid;
    }
}
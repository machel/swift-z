<?php

declare(strict_types=1);

namespace Common\Exception;

abstract class ContextException extends \Exception
{
    private array $context;
    private string $messageWithContext;

    /**
     * @throws \JsonException
     */
    public function __construct(
        string $message,
        array $context = [],
        int $code = 0,
        ?\Throwable $previous = null
    ) {
        $this->context = $context;
        $this->messageWithContext = $message . json_encode($context, JSON_THROW_ON_ERROR);
        parent::__construct($message, $code, $previous);
    }

    public function getContext(): array
    {
        return $this->context;
    }

    public function getMessageWithContext(): string
    {
        return $this->messageWithContext;
    }
}
<?php

declare(strict_types=1);

namespace Common\Exception;

class InvalidCommandInputException extends ContextException
{
}

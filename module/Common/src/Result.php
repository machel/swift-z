<?php

declare(strict_types=1);

namespace Common;

class Result
{
    use MessegeableTrait;

    private const GLOBAL_ERRID = '__global__';

    private string $firstError;

    public function addError(string $error, string|null $errid = null): self
    {
        $this->addErrors([$error], $errid);

        return $this;
    }

    public function addErrors(array $errors, string|null $errid = null): self
    {
        if (count($errors) < 1) {
            return $this;
        }

        if (!isset($this->firstError)) {
            $this->firstError = reset($errors);
        }

        $errid = $this->computeErrid($errid);

        $this->addMessages($errors, $errid);

        return $this;
    }

    public function setErrors(array $groupedErrors): self
    {
        $this->setMessages($groupedErrors);

        return $this;
    }

    public function hasErrors(string|null $errid = null): bool
    {
        return $this->hasMessages($errid);
    }

    public function getErrors(string|null $errid = null): array
    {
        $errid = $this->computeErrid($errid);
        return $this->getMessages($errid);
    }

    public function getAllErrors(): array
    {
        return $this->getAllMessages();
    }

    public function getFirstError(): string
    {
        return $this->firstError;
    }

    private function computeErrid(string|null $errid): string
    {
        return $errid ?? self::GLOBAL_ERRID;
    }
}
<?php

declare(strict_types=1);

namespace Common;

trait MessegeableTrait
{
    private array $messages = [];

    public function addMessage(string $message, string $msgid): self
    {
        $this->addMessages([$message], $msgid);

        return $this;
    }

    public function addMessages(array $messages, string $msgid): self
    {
        if (count($messages) < 1) {
            return $this;
        }

        if (!isset($this->messages[$msgid])) {
            $this->messages[$msgid] = [];
        }

        foreach ($messages as $message) {
            $this->messages[$msgid][] = $message;
        }

        return $this;
    }

    public function setMessages(array $groupedMessages): self
    {
        foreach ($groupedMessages as $msgid => $messages) {
            if (!is_array($messages)) {
                $messages = [$messages];
            }
            $this->addMessages($messages, $msgid);
        }

        return $this;
    }

    public function hasMessages(string|null $msgid = null): bool
    {
        if ($msgid === null) {
            return count($this->messages) > 0;
        }

        return
            isset($this->messages[$msgid]) &&
            count($this->messages[$msgid]) > 0;
    }

    public function getMessages(string $msgid): array
    {
        if (!isset($this->messages[$msgid])) {
            return [];
        }

        return $this->messages[$msgid];
    }

    public function getAllMessages(): array
    {
        return $this->messages;
    }
}
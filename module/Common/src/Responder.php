<?php

declare(strict_types=1);

namespace Common;

use Laminas\Stdlib\ResponseInterface;

class Responder
{
    public const OK = 200;
    public const BAD_REQUEST = 400;
    public const UNAUTHORIZED = 401;
    public const INTERNAL_SERVER_ERROR = 500;

    private const DEFAULT_OK_CODE = self::OK;

    private bool $success = true;
    private int $httpCode = self::DEFAULT_OK_CODE;
    private string $code;
    private string $message;
    private array $data;

    public function ok(int $httpCode = self::DEFAULT_OK_CODE): self
    {
        $this->success = true;
        $this->httpCode = $httpCode;

        return $this;
    }

    public function fail(int $httpCode): self
    {
        $this->success = false;
        $this->httpCode = $httpCode;

        return $this;
    }

    public function message(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function data(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function code(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function send(): ResponseInterface
    {
        $response = new \Laminas\Http\PhpEnvironment\Response();

        $response->setStatusCode($this->httpCode);

        $content = [
            'success' => $this->success,
        ];
        if (isset($this->message)) {
            $content['message'] = $this->message;
        }
        if (isset($this->data)) {
            $content['data'] = $this->data;
        }
        if (isset($this->code)) {
            $content['code'] = $this->code;
        }

        $response->setContent(value: json_encode($content));

        return $response;
    }
}
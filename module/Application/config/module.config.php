<?php

declare(strict_types=1);

namespace Application;

use Application\Config;
use Laminas\Mail\Transport\File as FileTransport;
use Application\Entity\User\Factory\UserFactory;
use Application\Entity\User\User;
use Application\Entity\User\Validator\UserPasswordValidatorInterface;
use Application\Service\Email\EmailerInterface;
use Application\Service\Email\FileEmailer;
use Doctrine\ORM\EntityManager;
use Interop\Container\Containerinterface;
use Laminas\Authentication\AuthenticationServiceInterface;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\Pages\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'application' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/application[/:action]',
                    'defaults' => [
                        'controller' => Controller\Pages\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'sign-in' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/sign-in',
                    'defaults' => [
                        'controller' => Controller\SignInController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'sign-out' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/sign-out',
                    'defaults' => [
                        'controller' => Controller\SignOutController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'register' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/register',
                    'defaults' => [
                        'controller' => Controller\RegisterController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'password-change' => [
                'type'    => Segment::class,
                'options' => [
                    'route'    => '/password-change[/:userId/:reason]',
                    'defaults' => [
                        'controller' => Controller\Pages\PasswordChangeController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'change-password' => [
                'type'    => Literal::class,
                'options' => [
                    'route'    => '/api/change-password',
                    'defaults' => [
                        'controller' => Controller\ChangePasswordController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\Pages\IndexController::class => Controller\Factory\Pages\IndexControllerFactory::class,
            Controller\Pages\PasswordChangeController::class => Controller\Factory\Pages\PasswordChangeControllerFactory::class,
            Controller\SignInController::class => Controller\Factory\SignInControllerFactory::class,
            Controller\SignOutController::class => Controller\Factory\SignOutControllerFactory::class,
            Controller\RegisterController::class => Controller\Factory\RegisterControllerFactory::class,
            Controller\ChangePasswordController::class => Controller\Factory\ChangePasswordControllerFactory::class,
        ],
    ],
    'service_manager' => [
        'factories' => [
            AuthenticationServiceInterface::class => static function ($container) {
                return $container->get('doctrine.authenticationservice.orm_default');
            },
            Config\Domain::class => static function(ContainerInterface $container, $requestedName) {
                return new Config\Domain($container->get('config')['domain']);
            },
            UseCase\SignInUser\SignInUserInterface::class => UseCase\SignInUser\Factory\SignInUserFactory::class,
            UseCase\SignOutUser\SignOutUserInterface::class => UseCase\SignOutUser\Factory\SignOutUserFactory::class,
            UseCase\RegisterUser\RegisterUserInterface::class => UseCase\RegisterUser\Factory\RegisterUserFactory::class,
            UseCase\GetSignedInUser\GetSignedInUserInterface::class => UseCase\GetSignedInUser\Factory\GetSignedInUserFactory::class,
            UseCase\GetRegisteredUsers\GetRegisteredUsersInterface::class => UseCase\GetRegisteredUsers\Factory\GetRegisteredUsersFactory::class,
            UseCase\GetUser\GetUserInterface::class => UseCase\GetUser\Factory\GetUserFactory::class,
            UseCase\IsUserPasswordToChange\IsUserPasswordToChangeInterface::class => UseCase\IsUserPasswordToChange\Factory\IsUserPasswordToChangeFactory::class,
            UseCase\ChangeUserPassword\ChangeUserPasswordInterface::class => UseCase\ChangeUserPassword\Factory\ChangeUserPasswordFactory::class,
            Entity\User\Validator\UserPasswordValidatorInterface::class => Entity\User\Validator\Factory\UserPasswordValidatorFactory::class,
            Repository\UserRepository::class => Repository\Factory\UserRepositoryFactory::class,
            UserFactory::class => static function(ContainerInterface $container, $requestedName) {
                return new UserFactory($container->get(UserPasswordValidatorInterface::class));
            },
            EmailerInterface::class => static function(ContainerInterface $container, $requestedName) {
                return new FileEmailer(
                    $container->get(Config\Domain::class),
                    new FileTransport()
                );
            }
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            // defines an annotation driver with two paths, and names it `my_annotation_driver`
            'my_annotation_driver' => [
                'class' => \Doctrine\ORM\Mapping\Driver\AttributeDriver::class,
                'cache' => 'array',
                'paths' => [
                    __DIR__ . '/../src/Entity',
                ],
            ],

            // default metadata driver, aggregates all other drivers into a single one.
            // Override `orm_default` only if you know what you're doing
            'orm_default' => [
                'drivers' => [
                    // register `my_annotation_driver` for any entity under namespace `My\Namespace`
                    'Application\Entity' => 'my_annotation_driver',
                ],
            ],
        ],
        'authentication' => [
            'orm_default' => [
                'object_manager' => EntityManager::class,
                'identity_class' => User::class,
                'identity_property' => 'email',
                'credential_property' => 'password',
                //'credential_callable' => \Application\Service\DoctrineAuthenticationHelper::class,
                //'credential_callable' => 'Application\Controller\UserController::verifyCredential',
                'credential_callable' => static function (User $user, $passwordGiven) {
                    return (new \Application\Service\DoctrineAuthenticationHelper())
                        ->verifyCredentials($user, $passwordGiven);
                },
            ],
        ],
    ],
];

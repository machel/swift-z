<?php

declare(strict_types=1);

namespace Application\UseCase\RegisterUser\Factory;

use Application\Entity\User\Factory\UserFactory;
use Application\Repository\UserRepository;
use Application\UseCase\RegisterUser\RegisterUser;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RegisterUserFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RegisterUser
    {
        return new RegisterUser(
            $container->get(UserFactory::class),
            $container->get(UserRepository::class)
        );
    }
}
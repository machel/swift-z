<?php

declare(strict_types=1);

namespace Application\UseCase\RegisterUser;

use Application\Entity\User\Factory\UserFactory;
use Application\Repository\UserRepository;
use Common\Exception\UnexpectedErrorException;

readonly class RegisterUser implements RegisterUserInterface
{
    public function __construct(
        private UserFactory $userFactory,
        private UserRepository $userRepository,
    ) {}

    /**
     * @throws UnexpectedErrorException
     */
    public function execute(Input $input): Output
    {
        $userResult = $this->userFactory->create($input->email(), $input->password(), $input->passwordRepeat());

        if ($userResult->errors()) {
            return new Output(
                $input->email(),
                new Error(ErrorType::USER_CREDENTIALS_INVALID, $userResult->errors())
            );
        }

        $user = $userResult->user();

        if ($user === null) {
            throw new UnexpectedErrorException();
        }

        $this->userRepository->addUser($user);

        return new Output(
            $input->email()
        );
    }
}
<?php

declare(strict_types=1);

namespace Application\UseCase\RegisterUser;

interface InputInterface
{
    public function email(): string;
    public function password(): string;
    public function passwordRepeat(): string;
}
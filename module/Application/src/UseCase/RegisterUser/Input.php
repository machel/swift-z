<?php

declare(strict_types=1);

namespace Application\UseCase\RegisterUser;

use Common\Exception\InvalidCommandInputException;

readonly class Input
{
    private string $email;
    private string $password;
    private string $passwordRepeat;

    private function __construct(
        string $email,
        string $password,
        string $passwordRepeat,
    ) {
        $this->email = $email;
        $this->password = $password;
        $this->passwordRepeat = $passwordRepeat;
    }

    /**
     * @throws InvalidCommandInputException
     */
    public static function create(array $data): self
    {
        if (!isset(
            $data['email'],
            $data['password'],
            $data['password_repeat']
        )) {
            throw new InvalidCommandInputException('Missing input');
        }

        return new self(
            $data['email'],
            $data['password'],
            $data['password_repeat']
        );
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function passwordRepeat(): string
    {
        return $this->passwordRepeat;
    }
}
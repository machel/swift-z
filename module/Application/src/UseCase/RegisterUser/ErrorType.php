<?php

namespace Application\UseCase\RegisterUser;

enum ErrorType: string
{
    case UNKNOWN = 'unknown';
    case USER_CREDENTIALS_INVALID = 'user_credentials_invalid';
}

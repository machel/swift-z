<?php

declare(strict_types=1);

namespace Application\UseCase\RegisterUser;

use Common\Exception\UnexpectedErrorException;

interface RegisterUserInterface
{
    /**
     * @throws UnexpectedErrorException
     */
    public function execute(Input $input): Output;
}
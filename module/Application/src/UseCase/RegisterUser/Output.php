<?php

declare(strict_types=1);

namespace Application\UseCase\RegisterUser;

use Common\Result;

class Output extends Result
{
    public function __construct(
        private readonly string $email,
        private readonly Error|null $error = null,
    ) {}

    public function successful(): bool
    {
        return $this->error === null;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function getError(): Error|null
    {
        return $this->error;
    }
}
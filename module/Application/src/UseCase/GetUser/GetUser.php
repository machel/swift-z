<?php

declare(strict_types=1);

namespace Application\UseCase\GetUser;

use Application\Entity\User\User;
use Application\Repository\UserRepository;

readonly class GetUser implements GetUserInterface
{
    public function __construct(
        private UserRepository $userRepository,
    ) {}

    public function execute(Input $input): Output
    {
        /**
         * @var User $user
         */
        $user = $this->userRepository->findOneBy($input->criteria());

        return new Output($user);
    }
}
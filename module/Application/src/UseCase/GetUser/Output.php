<?php

declare(strict_types=1);

namespace Application\UseCase\GetUser;

use Application\Entity\User\User;
use Common\Result;

class Output extends Result
{
    public function __construct(
        private readonly User|null $user,
    ) {}

    public function user(): User|null
    {
        return $this->user;
    }
}
<?php

namespace Application\UseCase\GetUser;

interface GetUserInterface
{
    public function execute(Input $input): Output;
}
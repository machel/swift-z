<?php

declare(strict_types=1);

namespace Application\UseCase\GetUser;

readonly class Input
{
    private function __construct(
        private int $id,
    ) {}

    public static function create(int $id): self
    {
        return new self($id);
    }

    public function criteria(): array
    {
        return [
            'id' => $this->id,
        ];
    }
}
<?php

declare(strict_types=1);

namespace Application\UseCase\GetUser\Factory;

use Application\Repository\UserRepository;
use Application\UseCase\GetUser\GetUser;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class GetUserFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): GetUser
    {
        return new GetUser(
            $container->get(UserRepository::class)
        );
    }
}
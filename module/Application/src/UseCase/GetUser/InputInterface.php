<?php

declare(strict_types=1);

namespace Application\UseCase\GetUser;

interface InputInterface
{
    public function criteria(): array;
}
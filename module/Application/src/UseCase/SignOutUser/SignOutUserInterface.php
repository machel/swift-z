<?php

declare(strict_types=1);

namespace Application\UseCase\SignOutUser;
use Common\Exception\UnexpectedErrorException;

interface SignOutUserInterface
{
    /**
     * @throws UnexpectedErrorException
     */
    public function execute(): Output;
}
<?php

declare(strict_types=1);

namespace Application\UseCase\SignOutUser;

use Application\Entity\User\User;
use Laminas\Authentication\AuthenticationServiceInterface;

readonly class SignOutUser implements SignOutUserInterface
{
    public function __construct(
        private AuthenticationServiceInterface $authenticationService
    ) {}

    public function execute(): Output
    {
        /**
         * @var User|null $user
         */
        $user = null;

        if ($this->authenticationService->hasIdentity()) {
            $user = $this->authenticationService->getIdentity();
        }

        $this->authenticationService->clearIdentity();

        return new Output($user);
    }
}

<?php

declare(strict_types=1);

namespace Application\UseCase\SignOutUser;

use Application\Entity\User\User;
use Common\Result;

class Output extends Result
{
    public function __construct(
        private readonly null|User $user = null
    ) {}

    public function getSignedOutUser(): null|User
    {
        return $this->user;
    }
}

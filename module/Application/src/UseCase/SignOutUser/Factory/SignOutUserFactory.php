<?php

declare(strict_types=1);

namespace Application\UseCase\SignOutUser\Factory;

use Application\UseCase\SignOutUser\SignOutUser;
use Interop\Container\ContainerInterface;
use Laminas\Authentication\AuthenticationServiceInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SignOutUserFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): SignOutUser
    {
        return new SignOutUser(
            $container->get(AuthenticationServiceInterface::class)
        );
    }
}

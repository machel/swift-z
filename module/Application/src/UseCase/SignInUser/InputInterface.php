<?php

declare(strict_types=1);

namespace Application\UseCase\SignInUser;

interface InputInterface
{
    public function email(): string;
    public function password(): string;
    public function isIgnorePasswordChange(): bool;
}
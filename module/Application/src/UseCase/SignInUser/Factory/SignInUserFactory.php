<?php

declare(strict_types=1);

namespace Application\UseCase\SignInUser\Factory;

use Application\UseCase\IsUserPasswordToChange\IsUserPasswordToChangeInterface;
use Application\UseCase\SignInUser\SignInUser;
use Interop\Container\ContainerInterface;
use Laminas\Authentication\AuthenticationServiceInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SignInUserFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): SignInUser
    {
        return new SignInUser(
            $container->get(IsUserPasswordToChangeInterface::class),
            $container->get(AuthenticationServiceInterface::class)
        );
    }
}

<?php

declare(strict_types=1);

namespace Application\UseCase\SignInUser;

use Application\UseCase\IsUserPasswordToChange\ChangeReason;
use Common\Result;

class Output extends Result
{
    public function __construct(
        private readonly bool $success,
        private readonly string $email,
        private readonly int|null $changePasswordForUser = null,
        private readonly ChangeReason|null $changeReason = null,
    ) {}

    public function successful(): bool
    {
        return $this->success;
    }

    public function email(): string
    {
        return $this->email;
    }

    public function changePassword(): bool
    {
        return $this->changePasswordForUser !== null;
    }

    public function changeReason(): ChangeReason|null
    {
        return $this->changeReason;
    }

    public function userId(): int|null
    {
        return $this->changePasswordForUser;
    }
}
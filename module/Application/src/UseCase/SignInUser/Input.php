<?php

declare(strict_types=1);

namespace Application\UseCase\SignInUser;

use Common\Exception\InvalidCommandInputException;

class Input
{
    private function __construct(
        private string $email,
        private string $password,
        private readonly bool $ignorePasswordChange = false,
    ) {
        $this->email = trim($email);
        $this->password = trim($password);
    }

    /**
     * @throws InvalidCommandInputException
     */
    public static function create(?string $email, ?string $password, bool $ignorePasswordChange = false): self
    {
        if ($email === null || $password === null) {
            throw new InvalidCommandInputException('Invalid format', [
                'email' => $email,
                'password' => $password,
            ]);
        }

        return new self(
            $email,
            $password,
            $ignorePasswordChange
        );
    }

    public function email(): string
    {
        return $this->email;
    }

    public function password(): string
    {
        return $this->password;
    }

    public function isIgnorePasswordChange(): bool
    {
        return $this->ignorePasswordChange;
    }
}

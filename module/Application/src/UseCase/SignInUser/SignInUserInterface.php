<?php

declare(strict_types=1);

namespace Application\UseCase\SignInUser;

use Common\Exception\UnexpectedErrorException;

interface SignInUserInterface
{
    /**
     * @throws UnexpectedErrorException
     */
    public function execute(Input $input): Output;
}
<?php

declare(strict_types=1);

namespace Application\UseCase\SignInUser;

use Application\UseCase\IsUserPasswordToChange;
use Application\UseCase\IsUserPasswordToChange\IsUserPasswordToChangeInterface;
use Common\Exception\UnexpectedErrorException;
use Laminas\Authentication\AuthenticationServiceInterface;

readonly class SignInUser implements SignInUserInterface
{
    public function __construct(
        private IsUserPasswordToChangeInterface $isUserPasswordToChange,
        private AuthenticationServiceInterface $authenticationService
    ) {}

    /**
     * @throws UnexpectedErrorException
     */
    public function execute(Input $input): Output
    {
        if (!$input->isIgnorePasswordChange()) {
            $result = $this->isUserPasswordToChange->execute(
                IsUserPasswordToChange\Input::create($input->email())
            );
            if ($result->mustChangePassword()) {
                return new Output(
                    false,
                    $input->email(),
                    $result->userId(),
                    $result->changeReason()
                );
            }
        }

        $adapter = $this->authenticationService->getAdapter();

        if ($adapter === null) {
            throw new UnexpectedErrorException();
        }

        $adapter->setIdentity($input->email());
        $adapter->setCredential($input->password());

        $result = $this->authenticationService->authenticate();

        return new Output(
            $result->isValid(),
            $input->email()
        );
    }
}
<?php

declare(strict_types=1);

namespace Application\UseCase\GetRegisteredUsers;

use Application\Entity\User\User;
use Common\Result;

class Output extends Result
{
    /**
     * @var User[] $users
     */
    private array $users;

    /**
     * @param User[] $users
     */
    public function __construct(array $users)
    {
        $this->users = $users;
    }

    /**
     * @return User[]
     */
    public function getUsers(): array
    {
        return $this->users;
    }
}
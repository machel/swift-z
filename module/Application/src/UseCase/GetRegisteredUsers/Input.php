<?php

declare(strict_types=1);

namespace Application\UseCase\GetRegisteredUsers;

class Input
{
    public static function create(): self
    {
        return new self();
    }
}
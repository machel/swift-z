<?php

declare(strict_types=1);

namespace Application\UseCase\GetRegisteredUsers;

use Application\Entity\User\User;
use Application\Repository\UserRepository;

readonly class GetRegisteredUsers implements GetRegisteredUsersInterface
{
    public function __construct(
        private UserRepository $userRepository,
    ) {}

    public function execute(Input $input): Output
    {
        /**
         * @var User[] $users
         */
        $users = $this->userRepository->findAll();

        return new Output($users);
    }
}
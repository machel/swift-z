<?php

declare(strict_types=1);

namespace Application\UseCase\GetRegisteredUsers;

use Common\Exception\UnexpectedErrorException;

interface GetRegisteredUsersInterface
{
    /**
     * @throws UnexpectedErrorException
     */
    public function execute(Input $input): Output;
}
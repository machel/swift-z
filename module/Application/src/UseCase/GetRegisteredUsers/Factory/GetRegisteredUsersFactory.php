<?php

declare(strict_types=1);

namespace Application\UseCase\GetRegisteredUsers\Factory;

use Application\Repository\UserRepository;
use Application\UseCase\GetRegisteredUsers\GetRegisteredUsers;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class GetRegisteredUsersFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): GetRegisteredUsers
    {
        return new GetRegisteredUsers(
            $container->get(UserRepository::class)
        );
    }
}
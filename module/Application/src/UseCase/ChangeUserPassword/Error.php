<?php

declare(strict_types=1);

namespace Application\UseCase\ChangeUserPassword;

use Common\MessegeableTrait;

class Error
{
    use MessegeableTrait;

    public function __construct(
        private readonly ErrorType $type,
        array|null $groupedErrors = null
    ) {
        if ($groupedErrors !== null) {
            $this->setMessages($groupedErrors);
        }
    }

    public function type(): ErrorType
    {
        return $this->type;
    }
}
<?php

declare(strict_types=1);

namespace Application\UseCase\ChangeUserPassword\Factory;

use Application\Config\Domain;
use Application\Entity\User\Validator\UserPasswordValidatorInterface;
use Application\Repository\UserRepository;
use Application\Service\Email\EmailerInterface;
use Application\UseCase\ChangeUserPassword\ChangeUserPassword;
use Application\UseCase\SignInUser\SignInUserInterface;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ChangeUserPasswordFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ChangeUserPassword
    {
        return new ChangeUserPassword(
            $container->get(Domain::class),
            $container->get(UserPasswordValidatorInterface::class),
            $container->get(SignInUserInterface::class),
            $container->get(UserRepository::class),
            $container->get(EmailerInterface::class),
        );
    }
}
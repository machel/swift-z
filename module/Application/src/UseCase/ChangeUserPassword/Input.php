<?php

declare(strict_types=1);

namespace Application\UseCase\ChangeUserPassword;

use Common\Exception\InvalidCommandInputException;

readonly class Input
{
    private function __construct(
        private string $email,
        private string $oldPassword,
        private string $newPassword,
        private string $newPasswordRepeat,
    ) {}

    /**
     * @throws InvalidCommandInputException
     */
    public static function create(
        ?string $email,
        ?string $oldPassword,
        ?string $newPassword,
        ?string $newPasswordRepeat,
    ): self {

        if (
            $email === null || $oldPassword === null || $newPassword === null || $newPasswordRepeat === null
        ) {
            throw new InvalidCommandInputException('Invalid format', [
                $email,
                $oldPassword,
                $newPassword,
                $newPasswordRepeat
            ]);
        }

        return new self(
            $email,
            $oldPassword,
            $newPassword,
            $newPasswordRepeat,
        );
    }

    public function email(): string
    {
        return $this->email;
    }

    public function oldPassword(): string
    {
        return $this->oldPassword;
    }

    public function newPassword(): string
    {
        return $this->newPassword;
    }

    public function newPasswordRepeat(): string
    {
        return $this->newPasswordRepeat;
    }
}
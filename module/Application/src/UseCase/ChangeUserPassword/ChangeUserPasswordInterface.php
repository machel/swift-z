<?php

declare(strict_types=1);

namespace Application\UseCase\ChangeUserPassword;

interface ChangeUserPasswordInterface
{
    public function execute(Input $input): Output;
}
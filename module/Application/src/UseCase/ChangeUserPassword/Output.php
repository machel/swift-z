<?php

declare(strict_types=1);

namespace Application\UseCase\ChangeUserPassword;

use Common\Result;

class Output extends Result
{
    public function __construct(
        private readonly bool $success,
        private readonly Error|null $error = null,
    ) {}

    public function successful(): bool
    {
        return $this->success;
    }

    public function getError(): Error|null
    {
        return $this->error;
    }
}
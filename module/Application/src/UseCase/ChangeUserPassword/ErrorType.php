<?php

namespace Application\UseCase\ChangeUserPassword;

enum ErrorType: string
{
    case UNKNOWN = 'unknown';
    case NEW_PASSWORD_INCORRECT = 'new_password_incorrect';
    case OLD_PASSWORD_INCORRECT = 'old_password_incorrect';
    case PASSWORD_FORMERLY_USED = 'password_formerly_used';
}
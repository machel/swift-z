<?php

declare(strict_types=1);

namespace Application\UseCase\ChangeUserPassword;

use Application\Config\Domain;
use Application\DomainService\PasswordProcessor;
use Application\Entity\User\User;
use Application\Entity\User\Validator\UserPasswordValidatorInterface;
use Application\Repository\UserRepository;
use Application\Service\Email\EmailerInterface;
use Application\Service\Email\TextMessage;
use Application\UseCase\SignInUser;
use Common\Exception\InvalidCommandInputException;
use Common\Exception\UnexpectedErrorException;

readonly class ChangeUserPassword implements ChangeUserPasswordInterface
{
    public function __construct(
        private Domain $config,
        private UserPasswordValidatorInterface $userPasswordValidator,
        private SignInUser\SignInUserInterface $signInUser,
        private UserRepository $userRepository,
        private EmailerInterface $emailer,
    ) {}

    /**
     * @throws UnexpectedErrorException
     * @throws InvalidCommandInputException
     */
    public function execute(Input $input): Output
    {
        //# does new password meet requirements
        $result = $this->userPasswordValidator->validate(
            $input->email(),
            $input->newPassword(),
            $input->newPasswordRepeat(),
        );
        if ($result->hasErrors()) {
            return new Output(false, new Error(ErrorType::NEW_PASSWORD_INCORRECT, $result->getAllErrors()));
        }

        //# is old password correct (sign-in)
        $command = SignInUser\Input::create(
            $input->email(),
            $input->oldPassword(),
            true,
        );
        $result = $this->signInUser->execute($command);
        if (!$result->successful()) {
            return new Output(false, new Error(ErrorType::OLD_PASSWORD_INCORRECT));
        }

        /**
         * @var User $user
         */
        $user = $this->userRepository->findOneBy(['email' => $input->email()]);

        //# was new password formerly used
        $nChecks = $this->config->checkAgainstNFormerPasswords();
        if ($user->hasUsedPasswordFormerly($input->newPassword(), $nChecks)) {
            return new Output(false, new Error(ErrorType::PASSWORD_FORMERLY_USED));
        }

        $user->changePassword(PasswordProcessor::hashPassword($input->newPassword()));
        $this->userRepository->saveUser($user);

        try {
            $message = $this->composeEmail($user->getEmail());
            $this->emailer->sendMessage($message);
        } catch (\Throwable $e) {
            // nic nie poradzimy, sie nie dowiedza
            // lepiej byloby dodac maila do jakiejs kolejki
        }

        return new Output(true);
    }

    private function composeEmail(string $toEmailAddress): TextMessage
    {
        $message = new TextMessage();
        $message->setData(
            'Simple Auth - password changed',
            'Your password has been changed. Team.',
            'figlarz@simple.auth',
            $toEmailAddress
        );

        return $message;
    }
}
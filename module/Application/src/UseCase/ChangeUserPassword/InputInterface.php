<?php

declare(strict_types=1);

namespace Application\UseCase\ChangeUserPassword;

interface InputInterface
{
    public function email(): string;
    public function oldPassword(): string;
    public function newPassword(): string;
    public function newPasswordRepeat(): string;
}
<?php

declare(strict_types=1);

namespace Application\UseCase\IsUserPasswordToChange;

use Common\Result;

class Output extends Result
{
    public function __construct(
        private readonly bool $mustChangePassword,
        private readonly ChangeReason|null $changeReason = null,
        private readonly int|null $userId = null,
    ) {}

    public function userId(): int|null
    {
        return $this->userId;
    }

    public function mustChangePassword(): bool
    {
        return $this->mustChangePassword;
    }

    public function changeReason(): ChangeReason|null
    {
        return $this->changeReason;
    }
}
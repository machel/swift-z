<?php

declare(strict_types=1);

namespace Application\UseCase\IsUserPasswordToChange;

use Application\Config\Domain;
use Application\Entity\User\User;
use Application\Repository\UserRepository;

readonly class IsUserPasswordToChange implements IsUserPasswordToChangeInterface
{
    public function __construct(
        private Domain $config,
        private UserRepository $userRepository,
    ) {}

    public function execute(Input $input): Output
    {
        $changePassword = false;
        $reason = null;

        /**
         * @var User $user
         */
        $user = $this->userRepository->findOneBy([
            'email' => $input->email(),
        ]);

        if ($user === null) {
            return new Output(false, null);
        }

        if ($user->getForcePasswordChange()) {
            $changePassword = true;
            $reason = ChangeReason::FIRST_SIGN_IN;
        } else if (($newestFormerPassword = $user->getNewestFormerPassword()) !== null) {
            $lastPasswordChange = $newestFormerPassword->getCreatedAt();
            $now = new \DateTime();

            $days = (int) $lastPasswordChange->diff($now)->format("%r%a");
            $expireDays = $this->config->passwordExpirationInDays();
            if ($days >= $expireDays) {
                $changePassword = true;
                $reason = ChangeReason::PASSWORD_EXPIRED;
            }
        }

        return new Output($changePassword, $reason, $user->getId());
    }
}
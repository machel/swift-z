<?php

declare(strict_types=1);

namespace Application\UseCase\IsUserPasswordToChange;

interface InputInterface
{
    public function email(): string;
}
<?php

declare(strict_types=1);

namespace Application\UseCase\IsUserPasswordToChange\Factory;

use Application\Config\Domain;
use Application\UseCase\IsUserPasswordToChange\IsUserPasswordToChange;
use Application\Repository\UserRepository;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class IsUserPasswordToChangeFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): IsUserPasswordToChange
    {
        return new IsUserPasswordToChange(
            $container->get(Domain::class),
            $container->get(UserRepository::class)
        );
    }
}
<?php

namespace Application\UseCase\IsUserPasswordToChange;

interface IsUserPasswordToChangeInterface
{
    public function execute(Input $input): Output;
}
<?php

declare(strict_types=1);

namespace Application\UseCase\IsUserPasswordToChange;

readonly class Input
{
    private string $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }

    public static function create(string $email): self
    {
        return new self($email);
    }

    public function email(): string
    {
        return $this->email;
    }
}
<?php

namespace Application\UseCase\IsUserPasswordToChange;

enum ChangeReason: string
{
    case FIRST_SIGN_IN = 'first_sign_in';
    case PASSWORD_EXPIRED = 'password_expired';
}
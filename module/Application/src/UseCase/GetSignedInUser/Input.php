<?php

declare(strict_types=1);

namespace Application\UseCase\GetSignedInUser;

readonly class Input
{
    public static function create(): self
    {
        return new self();
    }
}
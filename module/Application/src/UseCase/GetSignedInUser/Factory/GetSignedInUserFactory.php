<?php

declare(strict_types=1);

namespace Application\UseCase\GetSignedInUser\Factory;

use Application\UseCase\GetSignedInUser\GetSignedInUser;
use Laminas\Authentication\AuthenticationServiceInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class GetSignedInUserFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): GetSignedInUser
    {
        return new GetSignedInUser(
            $container->get(AuthenticationServiceInterface::class)
        );
    }
}
<?php

declare(strict_types=1);

namespace Application\UseCase\GetSignedInUser;

use Common\Exception\UnexpectedErrorException;

interface GetSignedInUserInterface
{
    /**
     * @throws UnexpectedErrorException
     */
    public function execute(Input $input): Output;
}
<?php

declare(strict_types=1);

namespace Application\UseCase\GetSignedInUser;

use Application\Entity\User\User;
use Common\Result;

class Output extends Result
{
    public function __construct(
        private readonly null|User $user = null
    ) {}

    public function isSignedIn(): bool
    {
        return $this->user !== null;
    }

    public function getUser(): null|User
    {
        return $this->user;
    }
}
<?php

declare(strict_types=1);

namespace Application\UseCase\GetSignedInUser;

use Application\Entity\User\User;
use Laminas\Authentication\AuthenticationServiceInterface;

readonly class GetSignedInUser implements GetSignedInUserInterface
{
    public function __construct(
        private AuthenticationServiceInterface $authenticationService
    ) {}

    public function execute(Input $input): Output
    {
        /**
         * @var User|null $user
         */
        $user = null;

        if ($this->authenticationService->hasIdentity()) {
            $user = $this->authenticationService->getIdentity();
        }

        return new Output($user);
    }
}
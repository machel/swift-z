<?php

declare(strict_types=1);

namespace Application\Entity\User;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ORM\Table(name: 'former_passwords')]
#[ORM\HasLifecycleCallbacks]
class FormerPassword
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int|null $id = null;

    #[ORM\Column(name: 'created_at' ,type: 'datetime', nullable: false)]
    protected \DateTime $createdAt;

    /** Many formerPasswords have one user. This is the owning side. */
    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'formerPasswords')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id')]
    private User|null $user = null;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private string $password;

    public function __construct(User $user, string $password)
    {
        $this->user = $user;
        $this->password = $password;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;
        return $this;
    }

    #[ORM\PrePersist]
    public function whenCreated(): void
    {
        $this->setCreatedAt(new \DateTime());
    }
}
<?php

namespace Application\Entity\User\Factory;

use Application\DomainService\PasswordProcessor;
use Application\Entity\User\Result\ErrorType;
use Application\Entity\User\Result\UserResult;
use Application\Entity\User\User;
use Application\Entity\User\Validator\UserPasswordValidatorInterface;

readonly class UserFactory
{
    public function __construct(
        private UserPasswordValidatorInterface $userPasswordValidator,
    ) {
    }

    public function create(string $email, string $password, string $passwordRepeat): UserResult
    {
        $result = $this->userPasswordValidator->validate($email, $password, $passwordRepeat);

        if ($result->hasErrors()) {
            return new UserResult(
                null,
                ErrorType::EMAIL_OR_PASSWORD_INCORRECT,
                $result->getAllErrors()
            );
        }

        return new UserResult(
            new User(
                $email,
                PasswordProcessor::hashPassword($password),
            )
        );
    }
}
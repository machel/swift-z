<?php

declare(strict_types=1);

namespace Application\Entity\User;

use Application\DomainService\PasswordProcessor;
use Application\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: 'users')]
#[ORM\HasLifecycleCallbacks]
class User
{
    #[ORM\Id]
    #[ORM\Column(type: 'integer')]
    #[ORM\GeneratedValue]
    private int|null $id = null;

    #[ORM\Column(name: 'created_at', type: 'datetime', nullable: false)]
    protected \DateTime $createdAt;

    #[ORM\Column(type: 'string', length: 255, unique: true, nullable: false)]
    private string $email;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private string $password;

    /**
     * One user has many formerPasswords. This is the inverse side.
     * @var Collection<int, FormerPassword>
     */
    #[ORM\OneToMany(mappedBy: 'user', targetEntity: FormerPassword::class, cascade: ["persist", "remove"])]
    #[ORM\OrderBy(["createdAt" => "DESC"])]
    private Collection $formerPasswords;

    #[ORM\Column(name: 'force_password_change', type: 'boolean', nullable: false, options: ["default" => false])]
    private bool $forcePasswordChange = false;

    public function __construct(string $email, string $hashedPassword)
    {
        $this->email = $email;
        $this->password = $hashedPassword;
        $this->formerPasswords = new ArrayCollection();
    }

    #[ORM\PrePersist]
    public function whenCreated(): void
    {
        $this->setCreatedAt(new \DateTime());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function getUsername(): string
    {
        return $this->email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail($email): self
    {
        $this->email = $email;
        return $this;
    }

    public function getDisplayName(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword($password): self
    {
        $this->password = $password;
        return $this;
    }

    public function getHash(): string
    {
        return $this->getPassword();
    }

    /**
     * @return Collection<int, FormerPassword>
     */
    public function getFormerPasswords(): Collection
    {
        return $this->formerPasswords;
    }

    /**
     * @param Collection<int, FormerPassword> $formerPasswords
     */
    public function setFormerPasswords(Collection $formerPasswords): self
    {
        $this->formerPasswords = $formerPasswords;
        return $this;
    }

    public function addFormerPassword(FormerPassword $formerPassword): self
    {
        $this->formerPasswords->add($formerPassword);
        return $this;
    }

    public function getNewestFormerPassword(): FormerPassword|null
    {
        $formerPasswords = $this->getFormerPasswords();
        if ($formerPasswords->count() < 1) {
            return null;
        }
        return $formerPasswords->first();
    }

    public function getForcePasswordChange(): bool
    {
        return $this->forcePasswordChange;
    }

    public function setForcePasswordChange(bool $forcePasswordChange): self
    {
        $this->forcePasswordChange = $forcePasswordChange;

        return $this;
    }

    public function hasUsedPasswordFormerly(string $password, int $nChecks = 100): bool
    {
        $n = 1;
        foreach ($this->getFormerPasswords() as $formerPassword) {
            if (PasswordProcessor::verifyPasswordAgainstHash($password, $formerPassword->getPassword())) {
                return true;
            }
            if ($n++ >= $nChecks) {   // check only last N passwords
                break;
            }
        }

        return false;
    }

    public function changePassword(string $hashedPassword): self
    {
        $this->addFormerPassword(new FormerPassword($this, $this->getPassword()));
        $this->setPassword($hashedPassword);
        $this->setForcePasswordChange(false);
        return $this;
    }
}
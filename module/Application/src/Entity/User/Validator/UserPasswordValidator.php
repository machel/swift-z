<?php

declare(strict_types=1);

namespace Application\Entity\User\Validator;

use Common\Result;
use Common\Validator\PasswordValidator;
use Laminas\Validator\EmailAddress;
use Application\Config\Domain;

readonly class UserPasswordValidator implements UserPasswordValidatorInterface
{
    public function __construct(
        private Domain $config
    ) {}

    public function validate(string $email, string $password, string $passwordRepeat): Result
    {
        $result = new Result();

        $validator = new EmailAddress();

        if (!$validator->isValid($email)) {
            $result->addError('Invalid email', 'email');
        }

        if ($password !== $passwordRepeat) {
            $result->addError('Passwords do not match', 'password');
            return $result;
        }

        $validator = new PasswordValidator($this->config->passwordRequirements());
        if (!$validator->isValid($password)) {
            $result->addErrors($validator->getMessages(), 'password');
        }

        return $result;
    }
}
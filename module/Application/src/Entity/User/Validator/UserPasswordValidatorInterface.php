<?php

namespace Application\Entity\User\Validator;

use Common\Result;

interface UserPasswordValidatorInterface
{
    public function validate(string $email, string $password, string $passwordRepeat): Result;
}
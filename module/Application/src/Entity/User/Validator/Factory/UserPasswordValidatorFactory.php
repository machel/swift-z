<?php

declare(strict_types=1);

namespace Application\Entity\User\Validator\Factory;

use Application\Config\Domain;
use Application\Entity\User\Validator\UserPasswordValidator;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserPasswordValidatorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserPasswordValidator
    {
        return new UserPasswordValidator($container->get(Domain::class));
    }
}
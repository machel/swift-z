<?php

declare(strict_types=1);

namespace Application\Entity\User\Result;

use Application\Entity\User\User;
use Common\MessegeableTrait;

class UserResult
{
    use MessegeableTrait;

    public function __construct(
        private readonly User|null $user,
        private readonly ErrorType|null $errorType = null,
        private readonly array|null $errors = null,
    ) {
        if ($errors !== null) {
            $this->setMessages($errors);
        }
    }

    public function user(): User|null
    {
        return $this->user;
    }

    public function errorType(): ErrorType|null
    {
        return $this->errorType;
    }

    public function errors(): array|null
    {
        return $this->getAllMessages();
    }
}
<?php

declare(strict_types=1);

namespace Application\Entity\User\Result;

enum ErrorType: string
{
    case UNKNOWN = 'unknown';
    case EMAIL_OR_PASSWORD_INCORRECT = 'email_or_password_incorrect';
}

<?php

declare(strict_types=1);

namespace Application\Service;

use Application\DomainService\PasswordProcessor;
use Application\Entity\User\User;

class DoctrineAuthenticationHelper
{
    public function verifyCredentials(User $user, $passwordGiven): bool
    {
        return PasswordProcessor::verifyPasswordAgainstHash($passwordGiven, $user->getHash());
    }
}
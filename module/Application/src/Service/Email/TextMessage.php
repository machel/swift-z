<?php

declare(strict_types=1);

namespace Application\Service\Email;

class TextMessage implements MessageInterface
{
    private string $subject;
    private string $body;
    private string $from;
    private string $to;

    public function setData(
        string $subject,
        string $body,
        string $from,
        string $to,
    ): void {
        $this->subject = $subject;
        $this->body = $body;
        $this->from = $from;
        $this->to = $to;
    }

    public function getEmail(): \Laminas\Mail\Message
    {
        $email = new \Laminas\Mail\Message();

        if (isset($this->subject)) {
            $email->setSubject($this->subject);
        }
        if (isset($this->body)) {
            $email->setBody($this->body);
        }
        if (isset($this->from)) {
            $email->setFrom($this->from);
        }
        if (isset($this->to)) {
            $email->addTo($this->to);
        }

        return $email;
    }
}
<?php

declare(strict_types=1);

namespace Application\Service\Email;

interface EmailerInterface
{
    public function sendMessage(MessageInterface $message);
}
<?php

declare(strict_types=1);

namespace Application\Service\Email;

use Application\Config\Domain;
use Laminas\Mail\Message as LaminasMessage;
use Laminas\Mail\Transport\File as FileTransport;
use Laminas\Mail\Transport\FileOptions;

readonly class FileEmailer implements EmailerInterface
{
    public function __construct(
        private Domain $config,
        private FileTransport $transport,
    ) {}

    public function sendMessage(MessageInterface $message): void
    {
        $this->sendMessageViaTransport($message->getEmail());
    }

    private function sendMessageViaTransport(LaminasMessage $message): void
    {
        $options   = new FileOptions([
            'path'     => $this->config->emailDirectory(),
            'callback' => function (FileTransport $transport) use ($message) {
                return sprintf(
                    'Message_%s_%f.txt',
                    str_replace(' ', '', mb_substr($message->getSubject(), 0, 20)),
                    microtime(true),
                );
            },
        ]);

        $this->transport->setOptions($options);
        $this->transport->send($message);
    }
}
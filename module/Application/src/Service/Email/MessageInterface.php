<?php

declare(strict_types=1);

namespace Application\Service\Email;

interface MessageInterface
{
    public function getEmail(): \Laminas\Mail\Message;
}
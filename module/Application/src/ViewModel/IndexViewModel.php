<?php

declare(strict_types=1);

namespace Application\ViewModel;

use Application\Entity\User\User;
use Laminas\View\Model\ViewModel;

class IndexViewModel extends ViewModel
{
    /**
     * @param User[]|null $registeredUsers
     */
    public function __construct(User|null $user, array|null $registeredUsers = null)
    {
        parent::__construct([
            'userEmail' => $user?->getEmail(),
            'registeredUsers' => $registeredUsers ?? [],
        ]);
    }
}
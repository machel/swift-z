<?php

declare(strict_types=1);

namespace Application\ViewModel;

use Laminas\View\Model\ViewModel;

class PasswordChangeViewModel extends ViewModel
{
    public function __construct(string|null $email, string|null $reasonMessage)
    {
        parent::__construct([
            'userEmail' => $email,
            'reason' => $reasonMessage,
        ]);
    }
}
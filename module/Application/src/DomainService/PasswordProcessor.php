<?php

namespace Application\DomainService;

class PasswordProcessor
{
    public static function verifyPasswordAgainstHash(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }

    public static function hashPassword(string $password): string
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
}
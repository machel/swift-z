<?php

declare(strict_types=1);

namespace Application\Repository\Factory;

use Application\Entity\User\User;
use Application\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Interop\Container\Containerinterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserRepositoryFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): UserRepository
    {
        return $container->get(EntityManager::class)->getRepository(User::class);
    }
}
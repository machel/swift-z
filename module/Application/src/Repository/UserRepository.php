<?php

declare(strict_types=1);

namespace Application\Repository;

use Application\Entity\User\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function addUser(User $user): void
    {
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
    }

    public function saveUser(User $user): void
    {
        $em = $this->getEntityManager();
        $em->flush();
    }
}
<?php

declare(strict_types=1);

namespace Application\Config;

readonly class Domain
{
    public function __construct(
        private array $config
    ) {}

    public function passwordRequirements(): array
    {
        return $this->config['password_requirements'];
    }

    public function checkAgainstNFormerPasswords(): int
    {
        return $this->config['check_new_password_against_n_former_passwords'];
    }

    public function passwordExpirationInDays(): int
    {
        return $this->config['password_expiration_in_days'];
    }

    public function emailDirectory(): string
    {
        return $this->config['email_directory'];
    }
}
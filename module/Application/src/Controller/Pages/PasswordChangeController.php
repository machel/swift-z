<?php

declare(strict_types=1);

namespace Application\Controller\Pages;

use Application\UseCase\IsUserPasswordToChange\ChangeReason;
use Application\ViewModel\PasswordChangeViewModel;
use Laminas\Mvc\Controller\AbstractActionController;
use Application\UseCase\GetUser;
use Laminas\View\Model\ViewModel;

class PasswordChangeController extends AbstractActionController
{
    public function __construct(
        private readonly GetUser\GetUser $getUser,
    ) {}

    public function indexAction(): ViewModel
    {
        $userId = $this->params()->fromRoute('userId', null);
        $userId = ($userId !== null) ? (int) $userId : null;
        $reason = $this->params()->fromRoute('reason', null);
        $email = null;
        $reasonMessage = '';

        if ($userId !== null) {
            $reasonMessage = $this->composeChangePasswordReasonMessage(ChangeReason::tryFrom($reason));
            $result = $this->getUser->execute(
                GetUser\Input::create($userId)
            );

            $email = $result->user()?->getEmail();
        }

        return new PasswordChangeViewModel($email, $reasonMessage);
    }

    private function composeChangePasswordReasonMessage(ChangeReason|null $changeReason): string
    {
        return match ($changeReason) {
            ChangeReason::FIRST_SIGN_IN => "It's your first sign-in attempt - please change your password",
            ChangeReason::PASSWORD_EXPIRED => "Your password expired",
            default => '',
        };
    }
}

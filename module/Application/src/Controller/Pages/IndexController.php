<?php

declare(strict_types=1);

namespace Application\Controller\Pages;

use Application\Entity\User\User;
use Application\UseCase\GetRegisteredUsers;
use Application\UseCase\GetSignedInUser;
use Application\ViewModel\IndexViewModel;
use Common\Exception\UnexpectedErrorException;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
//use Application\DomainService\PasswordProcessor;

class IndexController extends AbstractActionController
{
    public function __construct(
        private readonly GetSignedInUser\GetSignedInUserInterface $getSignedInUser,
        private readonly GetRegisteredUsers\GetRegisteredUsers $getRegisteredUsers,
    ) {}

    /**
     * @throws UnexpectedErrorException
     */
    public function indexAction(): ViewModel
    {
//        $pass = PasswordProcessor::hashPassword('pass');
//        dump($pass);
//        die();

        //dump(password_hash('pass', PASSWORD_DEFAULT));
        //dump(password_verify('pass', '$2y$10$uVImyvBaenpPp.6JEDifbeXj9je9efjPkKTg..DeHcTBcx2vDKn4.'));
        //die();
//        $hits = 0;
//        foreach (mb_str_split('AsDasaąść') as $char) {
//            if (!ctype_alnum($char)) {
//                continue;
//            }
//            if (mb_strtoupper($char) === $char)  {
//                ++$hits;
//            }
//            var_dump($char);
//        }
//        dump($hits);
//
//        $hits = 0;
//        foreach (mb_str_split('asD4saąść') as $char) {
//            if (!ctype_alnum($char)) {
//                continue;
//            }
//            if (mb_strtolower($char) === $char)  {
//                ++$hits;
//            }
//            var_dump($char);
//        }
//        dump($hits);
//
////        $w = preg_match_all('/[A-Z]/', 'asdasaąść');
////        dump($w);
////        $w = preg_match_all('/[a-z]/', 'asdasaąść');
////        dump($w);
//        $w = preg_match_all('/\d/', 'asdasaąść');
//        dump($w);
//        die();

        $query = GetSignedInUser\Input::create();
        $result = $this->getSignedInUser->execute($query);

        /**
         * @var User|null $user
         */
        $user= null;

        /**
         * @var User[]|null $registeredUsers
         */
        $registeredUsers = null;

        if ($result->isSignedIn()) {
            $user = $result->getUser();

            $query = GetRegisteredUsers\Input::create();
            $result = $this->getRegisteredUsers->execute($query);

            $registeredUsers = $result->getUsers();
        }

        return new IndexViewModel($user, $registeredUsers);
    }
}
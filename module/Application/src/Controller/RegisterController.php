<?php

declare(strict_types=1);

namespace Application\Controller;

use Application\UseCase\RegisterUser;
use Common\Exception\InvalidCommandInputException;
use Common\Exception\UnexpectedErrorException;
use Common\Responder;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Stdlib\ResponseInterface;

class RegisterController extends AbstractActionController
{
    public function __construct(
        private readonly Responder $responder,
        private readonly RegisterUser\RegisterUserInterface $registerUser,
    ) {}

    public function indexAction(): ResponseInterface
    {
        $data = $this->getRequest()->getPost();

        try {
            $command = RegisterUser\Input::create([
                'email' => $data->get('email'),
                'password' => $data->get('password'),
                'password_repeat' => $data->get('password_repeat'),
            ]);
            $result = $this->registerUser->execute($command);
        } catch (InvalidCommandInputException $e) {
            return $this->responder->fail(Responder::BAD_REQUEST)->message($e->getMessage())->send();
        } catch (UnexpectedErrorException|\Throwable $e) {
            return $this->responder->fail(Responder::INTERNAL_SERVER_ERROR)->send();
        }

        if (!$result->successful()) {
            $message = "Password do not match password's requirements.";
            return $this->responder->fail(Responder::BAD_REQUEST)->message($message)->send();
        }

        return $this->responder->ok()->data(['email' => $result->email()])->send();
    }
}
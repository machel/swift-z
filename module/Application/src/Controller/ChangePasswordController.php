<?php

declare(strict_types=1);

namespace Application\Controller;

use Common\Exception\InvalidCommandInputException;
use Common\Exception\UnexpectedErrorException;
use Common\Responder;
use Laminas\Mvc\Controller\AbstractActionController;
use Application\UseCase\ChangeUserPassword;

class ChangePasswordController extends AbstractActionController
{
    public function __construct(
        private readonly Responder $responder,
        private readonly ChangeUserPassword\ChangeUserPasswordInterface $changeUserPassword,
    ) {}

    public function indexAction()
    {
        $data = $this->getRequest()->getPost();
        try {
            $command = ChangeUserPassword\Input::create(
                $data->get('email'),
                $data->get('current_password'),
                $data->get('password'),
                $data->get('password_repeat'),
            );
            $result = $this->changeUserPassword->execute($command);
            if (!$result->successful()) {
                $message = $this->composeChangePasswordErrorMessage($result->getError()?->type());
                $errId = $result->getError()?->type()->value;
                return $this->responder->fail(Responder::BAD_REQUEST)->message($message)->code($errId)->send();
            }
        } catch (InvalidCommandInputException $e) {
            return $this->responder->fail(Responder::BAD_REQUEST)->message($e->getMessage())->send();
        } catch (UnexpectedErrorException|\Throwable $e) {
            return $this->responder->fail(Responder::INTERNAL_SERVER_ERROR)->send();
        }

        return $this->responder->ok()->data(['email' => $data->get('email')])->send();
    }

    private function composeChangePasswordErrorMessage(ChangeUserPassword\ErrorType $errorType): string
    {
        return match ($errorType) {
            ChangeUserPassword\ErrorType::OLD_PASSWORD_INCORRECT => "Authentication failed, email or current password incorrect.",
            ChangeUserPassword\ErrorType::NEW_PASSWORD_INCORRECT => "New password do not match password's requirements.",
            ChangeUserPassword\ErrorType::PASSWORD_FORMERLY_USED => "New password used before, please choose other password.",
            default => '',
        };
    }
}
<?php

declare(strict_types=1);

namespace Application\Controller;

use Application\UseCase\SignInUser;
use Common\Exception\InvalidCommandInputException;
use Common\Exception\UnexpectedErrorException;
use Common\Responder;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Stdlib\ResponseInterface;

class SignInController extends AbstractActionController
{
    public function __construct(
        private readonly Responder $responder,
        private readonly SignInUser\SignInUserInterface $signInUser,
    ) {}

    public function indexAction(): ResponseInterface
    {
        $data = $this->getRequest()->getPost();

        try {
            $command = SignInUser\Input::create(
                $data->get('email'),
                $data->get('password'),
            );
            $result = $this->signInUser->execute($command);
            if ($result->changePassword()) {

                $responseData = [
                    'user_id' => $result->userId(),
                    'change_password' => true,
                    'change_reason' => $result->changeReason()->value,
                ];
                return $this->responder->ok()->data($responseData)->send();
            }

        } catch (InvalidCommandInputException $e) {
            return $this->responder->fail(Responder::BAD_REQUEST)->message($e->getMessage())->send();
        } catch (UnexpectedErrorException|\Throwable $e) {
            return $this->responder->fail(Responder::INTERNAL_SERVER_ERROR)->send();
        }

        if (!$result->successful()) {
            $message = 'Authorization failed';
            return $this->responder->fail(Responder::UNAUTHORIZED)->message($message)->send();
        }

        return $this->responder->ok()->data(['email' => $data->get('email')])->send();
    }
}
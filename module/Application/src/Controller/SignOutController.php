<?php

declare(strict_types=1);

namespace Application\Controller;

use Common\Exception\UnexpectedErrorException;
use Common\Responder;
use Application\UseCase\SignOutUser;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Stdlib\ResponseInterface;

class SignOutController extends AbstractActionController
{
    public function __construct(
        private readonly Responder $responder,
        private readonly SignOutUser\SignOutUserInterface $signOutUser,
    ) {}

    public function indexAction(): ResponseInterface
    {
        try {
            $result = $this->signOutUser->execute();
        } catch (UnexpectedErrorException|\Throwable $e) {
            return $this->responder->fail(Responder::INTERNAL_SERVER_ERROR)->send();
        }

        return $this->responder->ok()->data(['email' => $result->getSignedOutUser()?->getEmail()])->send();
    }
}
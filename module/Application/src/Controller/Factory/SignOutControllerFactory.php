<?php

declare(strict_types=1);

namespace Application\Controller\Factory;

use Application\Controller\SignOutController;
use Application\UseCase;
use Common\Responder;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SignOutControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): SignOutController
    {
        return new SignOutController(
            $container->get(Responder::class),
            $container->get(UseCase\SignOutUser\SignOutUserInterface::class)
        );
    }
}

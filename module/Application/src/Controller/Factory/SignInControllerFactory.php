<?php

declare(strict_types=1);

namespace Application\Controller\Factory;

use Application\Controller\SignInController;
use Application\UseCase;
use Common\Responder;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class SignInControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): SignInController
    {
        return new SignInController(
            $container->get(Responder::class),
            $container->get(UseCase\SignInUser\SignInUserInterface::class)
        );
    }
}

<?php

declare(strict_types=1);

namespace Application\Controller\Factory;

use Application\Controller\ChangePasswordController;
use Application\UseCase\ChangeUserPassword\ChangeUserPasswordInterface;
use Common\Responder;
use Interop\Container\ContainerInterface;

class ChangePasswordControllerFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): ChangePasswordController
    {
        return new ChangePasswordController(
            $container->get(Responder::class),
            $container->get(ChangeUserPasswordInterface::class)
        );
    }
}
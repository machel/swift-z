<?php

declare(strict_types=1);

namespace Application\Controller\Factory\Pages;

use Application\Controller\Pages\PasswordChangeController;
use Application\UseCase;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PasswordChangeControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): PasswordChangeController
    {
        return new PasswordChangeController(
            $container->get(UseCase\GetUser\GetUserInterface::class),
        );
    }
}

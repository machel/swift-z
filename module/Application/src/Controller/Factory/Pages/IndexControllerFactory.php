<?php

declare(strict_types=1);

namespace Application\Controller\Factory\Pages;

use Application\Controller\Pages\IndexController;
use Application\UseCase;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): IndexController
    {
        return new IndexController(
            $container->get(UseCase\GetSignedInUser\GetSignedInUserInterface::class),
            $container->get(UseCase\GetRegisteredUsers\GetRegisteredUsersInterface::class),
        );
    }
}

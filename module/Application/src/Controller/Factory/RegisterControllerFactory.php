<?php

declare(strict_types=1);

namespace Application\Controller\Factory;

use Application\Controller\RegisterController;
use Application\UseCase;
use Common\Responder;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RegisterControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): RegisterController
    {
        return new RegisterController(
            $container->get(Responder::class),
            $container->get(UseCase\RegisterUser\RegisterUserInterface::class)
        );
    }
}
